#pragma once

#ifdef  JGMRDR_EXPORTS 
#define JGMRDR_DLLCALL __declspec(dllexport)   /* Should be enabled before compiling 
                                           .dll project for creating .dll*/
#else
#define JGMRDR_DLLCALL __declspec(dllimport)  /* Should be enabled in Application side
                                          for using already created .dll*/
#endif

#include <string>

// Interface Class
class JGMitemInterface {
public:
    virtual ~JGMitemInterface() {}
};

class JGMitem : public JGMitemInterface {
public:
	char *filePath;
	unsigned int headingAddress;
	unsigned int headingLength;
	unsigned int fileAddress;
	unsigned int fileLength;

	char *title;
	char *album;
	char *performer;
	char *origin;
	char *fileType;
	unsigned int duration;

	char *fileBuffer;

	JGMitem();
	~JGMitem();
};
