#include "jgmFile.h"
#include "jgmItem.h"
#include "..\..\jgp-std-math\jgp-std-math\JGPstdMath.h"
#include <fstream>

JGMfile::JGMfile(const char *filePath)
{
	this->filePath = filePath;
	loadFile(); // TODO obs�uga wyj�tku, je�li plik nie zostanie poprawnie za�adowany rzu� wyj�tek (throw)
	// http://stackoverflow.com/questions/1622546/constructor-abort-constructing
}

JGMfile::~JGMfile()
{
	for(std::vector<JGMitem*>::iterator it = itemList.begin(); it != itemList.end(); it++)
		delete (*it);
}

bool JGMfile::loadFile()
{
	char *strBuffer = NULL;
	short int strLength;
	std::ifstream fileHandle(filePath, std::ios::in | std::ios::binary);

	if(!fileHandle)
		return false;
	
	// liczba utwor�w i nazwa albumu
	fileHandle.read((char*)&songCount, sizeof(unsigned));
	fileHandle.read((char*)&strLength, sizeof(short int));
	strLength = jgp_std_math_convertEndianShort(strLength);
	strBuffer = new char[strLength + 1];
	fileHandle.read(strBuffer, strLength);
	strBuffer[strLength] = '\0';
	playlistTitle = strBuffer;
	
	// podstawowe informacje o strukturze pliku
	for(unsigned i = 0; i < songCount; i++)
	{
		itemList.push_back(new JGMitem());
		fileHandle.read((char*)&itemList[i]->headingAddress, sizeof(unsigned int));
		fileHandle.read((char*)&itemList[i]->headingLength, sizeof(unsigned int));
		fileHandle.read((char*)&itemList[i]->fileAddress, sizeof(unsigned int));
		fileHandle.read((char*)&itemList[i]->fileLength, sizeof(unsigned int));
	}
	
	// nag��wki plik�w
	for(unsigned i = 0; i < songCount; i++)
	{
		// tytu�
		fileHandle.read((char*)&strLength, sizeof(short int));
		strLength = jgp_std_math_convertEndianShort(strLength);
		strBuffer = new char[strLength + 1];
		fileHandle.read(strBuffer, strLength);
		strBuffer[strLength] = '\0';
		itemList[i]->title = strBuffer;
		// album
		fileHandle.read((char*)&strLength, sizeof(short int));
		strLength = jgp_std_math_convertEndianShort(strLength);
		strBuffer = new char[strLength + 1];
		fileHandle.read(strBuffer, strLength);
		strBuffer[strLength] = '\0';
		itemList[i]->album = strBuffer;
		// wykonawca
		fileHandle.read((char*)&strLength, sizeof(short int));
		strLength = jgp_std_math_convertEndianShort(strLength);
		strBuffer = new char[strLength + 1];
		fileHandle.read(strBuffer, strLength);
		strBuffer[strLength] = '\0';
		itemList[i]->performer = strBuffer;
		// pochodzenie
		fileHandle.read((char*)&strLength, sizeof(short int));
		strLength = jgp_std_math_convertEndianShort(strLength);
		strBuffer = new char[strLength + 1];
		fileHandle.read(strBuffer, strLength);
		strBuffer[strLength] = '\0';
		itemList[i]->origin = strBuffer;
		// typ pliku
		fileHandle.read((char*)&strLength, sizeof(short int));
		strLength = jgp_std_math_convertEndianShort(strLength);
		strBuffer = new char[strLength + 1];
		fileHandle.read(strBuffer, strLength);
		strBuffer[strLength] = '\0';
		itemList[i]->fileType = strBuffer;
	}
	
	// pliki audio
	for(unsigned i = 0; i < songCount; i++)
	{
		strBuffer = new char[itemList[i]->fileLength];
		fileHandle.read(strBuffer, itemList[i]->fileLength);
		itemList[i]->fileBuffer = strBuffer;
	}
	fileHandle.close();
	return true;
}

unsigned JGMfile::getSongCount()
{
	return itemList.size();
}

const char* JGMfile::getPlaylistTitle()
{
	return playlistTitle;
}

const char* JGMfile::getTitle(int id)
{
	return itemList[id]->title;
}

const char* JGMfile::getAlbum(int id)
{
	return itemList[id]->album;
}

const char* JGMfile::getPerformer(int id)
{
	return itemList[id]->performer;
}

const char* JGMfile::getOrigin(int id)
{
	return itemList[id]->origin;
}

const char* JGMfile::getFileType(int id)
{
	return itemList[id]->fileType;
}

unsigned int JGMfile::getDuration(int id)
{
	return itemList[id]->duration;
}

void JGMfile::setDuration(int id, unsigned int duration)
{
	itemList[id]->duration = duration;
}

const char* JGMfile::getFileBuffer(int id)
{
	return itemList[id]->fileBuffer;
}

unsigned int JGMfile::getFileLength(int id)
{
	return itemList[id]->fileLength;
}
