#include "jgmItem.h"

JGMitem::JGMitem()
{
	fileBuffer = NULL;
	duration = 0;
}

JGMitem::~JGMitem()
{
	if(fileBuffer)
		delete[] fileBuffer;
}
