#pragma once

#ifdef  JGMRDR_EXPORTS 
#define JGMRDR_DLLCALL __declspec(dllexport)   /* Should be enabled before compiling 
                                           .dll project for creating .dll*/
#else
#define JGMRDR_DLLCALL __declspec(dllimport)  /* Should be enabled in Application side
                                          for using already created .dll*/
#endif

#include "jgmFile.h"

// Interface Class
class JGMdllInterface {
public:
    virtual ~JGMdllInterface() {}
	virtual JGMfile* loadFile(const char *filePath) = 0;
};

// Concrete Class
class JGMfileReader: public JGMdllInterface {
private:
	const char *filePath;
public:
	JGMfileReader() {}
	JGMfile* loadFile(const char *filePath);
};


//  Factory function that will return the new object instance. (Only function
//  should be declared with DLLCALL)
extern "C" /*Important for avoiding Name decoration*/
{
	JGMRDR_DLLCALL JGMdllInterface* _cdecl CreateJGMreaderObject();
};

// Function Pointer Declaration of CreateJGMReaderObject() [Entry Point Function]
typedef JGMdllInterface* (*CREATE_JGM_INTERFACE) ();
