#include "jgmFileReader.h"
#include <Windows.h>

// Create Object
JGMRDR_DLLCALL JGMdllInterface* _cdecl CreateJGMreaderObject() {
    return new JGMfileReader();
}

JGMfile* JGMfileReader::loadFile(const char *filePath)
{
	return new JGMfile(filePath);
}