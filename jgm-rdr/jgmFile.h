#pragma once

#ifdef  JGMRDR_EXPORTS 
#define JGMRDR_DLLCALL __declspec(dllexport)   /* Should be enabled before compiling 
                                           .dll project for creating .dll*/
#else
#define JGMRDR_DLLCALL __declspec(dllimport)  /* Should be enabled in Application side
                                          for using already created .dll*/
#endif

#include <vector>

class JGMitem;

// Interface Class
class JGMfileInterface {
public:
    virtual ~JGMfileInterface() {}
	virtual unsigned getSongCount() = 0;
	virtual const char* getPlaylistTitle() = 0;
	virtual const char* getTitle(int id) = 0;
	virtual const char* getAlbum(int id) = 0;
	virtual const char* getPerformer(int id) = 0;
	virtual const char* getOrigin(int id) = 0;
	virtual const char* getFileType(int id) = 0;
	virtual unsigned int getDuration(int id) = 0;
	virtual void setDuration(int id, unsigned int duration) = 0;
	virtual const char* getFileBuffer(int id) = 0;
	virtual unsigned int getFileLength(int id) = 0;
};

class JGMfile : public JGMfileInterface {
private:
	const char *filePath;
	std::vector<JGMitem*> itemList;
	
	char *playlistTitle;
	unsigned songCount;

	bool loadFile();
public:
	JGMfile(const char *filePath);
	~JGMfile();
	
	unsigned getSongCount();
	const char* getPlaylistTitle();

	const char* getTitle(int id);
	const char* getAlbum(int id);
	const char* getPerformer(int id);
	const char* getOrigin(int id);
	const char* getFileType(int id);
	unsigned int getDuration(int id);
	void setDuration(int id, unsigned int duration);
	
	const char* getFileBuffer(int id);
	unsigned int getFileLength(int id);
};
